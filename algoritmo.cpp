#include <iostream>
#include <cstdlib>
#include <math.h>

// By Robson Maciel
// Sistema de controle de corrente elétrica para maquina de corte a plasma

using namespace std;

float espessura = 0, corrente = 0, velocidade = 0;
char op;

// Função para cálculo de corrente da maquina de corte a plasma
void CorrentControl() {
	corrente = (21 * log(espessura) + 37);
	velocidade = (-2485 * log(espessura) + 7832);
	
	cout << "\nAjustando a Corrente de operação" << endl;
	
	for(int x = 0; x < 3; x++) {
		cout << ". " << endl;
		system("sleep 1");
	}
	
	cout << "\t=> ESPESSURA DA CHAPA: " << espessura << " mm" << endl
		 << "\t=> CORRENTE DE OPERAÇÃO: " << floor(corrente) << " A" << endl
		 << "\t=> VELOCIDADE DO CORTE: " << floor(velocidade) << " mm/min" << endl;
	cout << "\nPressione qualquer tecla para voltar ao menu. ";
	getchar();
}

// Função principal
int main() {
	setlocale(LC_ALL, "Portuguese");
	
	while(true) {
		espessura = 0;
		corrente = 0;
		velocidade = 0;
		
		system("clear");
		cout << "===============================================================================" << endl;
		cout << "===================| SISTEMA DE CONTROLE DE CORTE A PLASMA |===================" << endl;
		cout << "===============================================================================" << endl;
		cout << "Medidas disponíveis:" << endl
			 << "\t[ 1 ]  0.5 mm" << endl
			 << "\t[ 2 ]  1.5 mm" << endl
			 << "\t[ 3 ]  3.0 mm" << endl
			 << "\t[ 4 ]  6.0 mm" << endl
			 << "\t[ 5 ] 12.0 mm" << endl
			 << "\t[ 6 ] 19.0 mm" << endl
			 << "\t[ 7 ] 25.0 mm" << endl
			 << "\t[ 8 ] SAIR" << endl
			 << "Informe a opção desejada: ";
		cin >> op;
		getchar();
		
		switch(op) {
			case '1':
				espessura = 0.5;
				CorrentControl();
				break;
			case '2':
				espessura = 1.5;
				CorrentControl();
				break;
			case '3':
				espessura = 3.0;
				CorrentControl();
				break;
			case '4':
				espessura = 6.0;
				CorrentControl();
				break;
			case '5':
				espessura = 12.0;
				CorrentControl();
				break;
			case '6':
				espessura = 19.0;
				CorrentControl();
				break;
			case '7':
				espessura = 20.0;
				CorrentControl();
				break;
			case '8':
				break;
			default:
				cout << "\n          +-+-+-+-+-+-+-+-+-+-+ Opção Inválida! +-+-+-+-+-+-+-+-+-+-+          " << endl;
				getchar();
				break;
		}
		if(op == '8') break;
	}
	
	return 0;
}
